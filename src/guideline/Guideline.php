<?php
 namespace Quickqc\guideline;
 use Quickqc\Utility;
	 class Guideline {
		public $title = '';
		public $description = '';
		public $example = '';
		public $status = '';
		public $created = '';
		public $modified = '';
		public $created_by = '';
		public $modified_by = '';
		public $deleted_at = '';
		public $data = array(); 		// this variable use for return value
		private $conn = ''; 			// this variable use for database connection
		
		function __construct(){
			$this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
			$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}	
		 
		public function create ($data=null){
			try {
				if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $stmt = $this->conn->prepare("INSERT INTO guidelines(title,description) VALUES(:title,:description)");
                    $stmt->bindParam(':title', $this->title);
                    $stmt->bindParam(':description', $this->description);
                    $this->title = $data['title'];
                    $this->description = $data['description'];
                    $stmt->execute();
					Utility :: redirect();
				}
				else {
					Utility :: redirect();
				}
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
	
			Utility :: redirect();

		}

		public function get ($id = null){
			try {
				
				$result = $this->conn->prepare('SELECT * FROM `guidelines` WHERE id = :id');
				$result->execute(array('id' => $id));
			 
				$row = $result->fetch();
				return $row;	
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
			
			
		}

		public function read ($limit=''){
			try {
				$query = "SELECT * FROM guidelines $limit";
				$result = $this->conn->query($query);

				foreach ($result as $row) {
					$this->data[] = $row;
				}
				return $this->data;
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
		}

		public function update ($data=null){

            try {
                $stmt = $this->conn->prepare('UPDATE guidelines SET title = :title,description=:description WHERE id = :id');
                $stmt->execute(array(
                    ':id'   => $data['id'],
                    ':title' => $data['title'],
                    ':description' => $data['description']
                ));
            } catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
            Utility::redirect();
		}

		public function delete ($id){

            try {
                $stmt = $this->conn->prepare('DELETE FROM guidelines WHERE id = :id');
                $stmt->bindParam(':id', $id); // this time, we'll use the bindParam method
                $stmt->execute();
            } catch(PDOException $e) {
                echo 'Error: ' . $e->getMessage();
            }
            Utility::redirect();
		}

	}