<?php
/**
 * Created by PhpStorm.
 * User: Nazmul Hasan
 * Date: 26-Jul-15
 * Time: 4:41 PM
 */
namespace Quickqc\pagination;
class Pagination
{
    public $conn = '';
    public $page_rows = '';
    public $pagenum = '';
    public $last = '';

    function __construct()
    {
        $this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
        $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function paginate()
    {
        try {
            $sql = "SELECT COUNT(id) FROM guidelines";
            $result = $this->conn->prepare($sql);
            $result->execute();
            $row = $result->fetchColumn();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        //this is the number of result we want to display perpage;
        $this->page_rows = 3;
        $this->last = ceil($row / $this->page_rows);
        //this make sure that $last cannot be less than 1
        if ($this->last < 1) {
            $this->last = 1;
        }
        //Established pagenum variable;
        $this->pagenum = 1;
        //Get pagenum from URL if it is present, else it is 1;
        if (isset($_GET['pn'])) {
            $this->pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
        }
        //This check that pagenum isn't below 1 or more than $last;
        if ($this->pagenum < 1)
            $this->pagenum = 1;
        else if ($this->pagenum > $this->last)
            $this->pagenum = $this->last;
        //Established the $paginationCtrls Variable;
        $paginationCtrls= '';
        $paginationCtrls.='<ul class="pagination">';
        if ($this->last != 1) {
            if ($this->pagenum > 1) {
                $previous = $this->pagenum - 1;
                $paginationCtrls.='<li>';
                $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '">Previous</a> &nbsp; &nbsp;';
                $paginationCtrls.='</li>';
                //render clickable number link that shoud appear on the left of the targated page number;
                for ($i = $this->pagenum - 4; $i < $this->pagenum; $i++) {
                    if ($i > 0) {
                        $paginationCtrls .= '<li>';
                        $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '</a> &nbsp;';
                        $paginationCtrls .= '</li>';
                    }
                }
            }
            //Render the targated page number, but without it being link;
            $paginationCtrls.='<li class="active">';
            $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $this->pagenum . '">' . $this->pagenum . '</a> &nbsp;';
            $paginationCtrls .= '</li>';
            //render clickable number link that shoud appear on the right of the targated page number;
            for ($i = $this->pagenum + 1; $i <= $this->last; $i++) {
                $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i . '">' . $i . '</a> &nbsp;</li>';
                if ($i >= $this->pagenum + 4)
                    break;
            }
            if ($this->pagenum != $this->last) {
                $next = $this->pagenum + 1;
                $paginationCtrls .= '<li><a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $next . '">Next</a></li></li>';
            }
            $paginationCtrls.='</ul>';
        }
        return $paginationCtrls;
    }

    public function get_limit()
    {
        try {
            $sql = "SELECT COUNT(id) FROM guidelines";
            $result = $this->conn->prepare($sql);
            $result->execute();
            $row = $result->fetchColumn();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        //this is the number of result we want to display perpage;
        $page_rows = 3;
        $this->last = ceil($row / $page_rows);
        //this make sure that $last cannot be less than 1
        if ($this->last < 1) {
            $this->last = 1;
        }
        //Established pagenum variable;
        $pagenum = 1;
        //Get pagenum from URL if it is present, else it is 1;
        if (isset($_GET['pn'])) {
            $this->pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
        }
        //This check that pagenum isn't below 1 or more than $last;
        if ($this->pagenum < 1)
            $this->pagenum = 1;
        else if ($pagenum > $this->last)
            $this->pagenum = $this->last;
        $limit = 'LIMIT ' . ($this->pagenum - 1) * $page_rows . ',' . $page_rows;
        return $limit;
    }
}