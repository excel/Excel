
<?php
include_once('../../inc/application.php');
$guidlines = $guidline->read();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Checklist | User Profile</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <!--<link href="css/profile.css" rel="stylesheet" type="text/css"/>
        <link href="css/tasks.css" rel="stylesheet" type="text/css"/>-->
        <link href="css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login body_color" >
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler">
        </div>
        <!-- START MENU OR HEADER OPTION -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="login.html">Checklist</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <div class="header_menu">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">HOME <span class="sr-only">(current)</span></a></li>
                            <li><a href="guide.php">GUIDELINE</a></li>
                            <li><a href="login.html">LOGOUT</a></li>
                            <li><a href="glossary.html">GLOSSARY</a></li>
                        </ul>
                    </div>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <!-- END HEADER -->
        <br/>
        <br/>
        <br/>
        <div class="container">
            <div class="row profile">
                <div class="col-md-3">
                    <div class="profile-sidebar">
                     
                        <!-- SIDEBAR MENU -->
                        <div class="profile-usermenu">
                            <ul class="nav">
                                <li>
                                    <a href="user_profile.html">
                                        <i class="fa fa-home fa-4x"></i>
                                        Dashboard </a>
                                </li>
                                <li>
                                    <a href="account_setting.html">
                                        <i class="fa fa-cog"></i>
                                        Account Settings </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal12">
                                        <i class="fa fa-pencil-square-o"></i>
                                        Add Project</a>
                                </li>
                                <li>
                                    <a href="manage_project.html">
                                        <i class="fa fa-list"></i>
                                        Manage Project</a>
                                </li>
                                <li class="active">
                                    <a href="index.php">
                                        <i class="fa fa-tags"></i>
                                        Manage Guidelines</a>
                                </li>
                                <li>
                                    <a href="manage_qc.html">
                                        <i class="fa fa-tags"></i>
                                        Manage QC</a>
                                </li>
                                   <li>
                                    <a href="#" data-toggle="modal" data-target="#myModal1">
                                        <i class="fa fa-spinner"></i>
                                        Suggest QC</a>
                                </li>

                            </ul>
                        </div>
                        <!-- END MENU -->
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="profile-content">
						<center><h2>Criteria Management</h2></center>
						<a href="insert.php" class="btn btn-success uppercase">Add new</a><br/><br/>
						<div class="table-responsive">
							<table class="table table-striped">
								<thead>
								  <tr>
									<th>Sl</th>
									<th>Title</th>
									<th>Description</th>
									<th>Action</th>
								  </tr>
								</thead>
								<tbody>
									<?php 
										if(!empty($guidlines)) {          
										foreach($guidlines as $key=>$value){    
									?>
									<tr>
										<td><?php echo $key+1;?></td>
										<td style="width:150px">
											<?php 
												if(array_key_exists('title', $value) && !empty($value['title'])){
													echo $value['title'];
												}
											 ?>
										</td>
										<td style="width:390px; text-align:justify;">
											<?php
												if(array_key_exists('description', $value) && !empty($value['description'])){
													echo $value['description'];
												}
											?>
										</td>
										<td>
											<a class="btn btn-success uppercase" href="show.php?id=<?php echo $value['id'];  ?>">View</a>
											<a class="btn btn-success uppercase" href="edit.php?id=<?php echo $value['id'];  ?>">Edit</a>
											<a class="btn btn-success uppercase" onclick="return confirme_delete();" href="delete.php?id=<?php echo $value['id'];  ?>">Delete</a>
										</td>
									</tr>
									<?php
									}
									}
									else{
									?>
									<tr>
										<td colspan="4">No record available here</td>
									</tr>
									<?php		
									}
									?>
								</tbody>
							</table>
						</div>
						<center>
							<nav>
								<ul class="pagination">
									<li class="disabled"><a aria-label="Previous" href="#"><span aria-hidden="true">«</span></a></li>
									<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li><a href="#">4</a></li>
									<li><a href="#">5</a></li>
									<li><a aria-label="Next" href="#"><span aria-hidden="true">»</span></a></li>
								</ul>
							</nav>						
						</center>
                    </div>
                </div> 
            </div>
        </div>
        <br/>
        <br/>

        <div class="footer navbar-fixed-bottom">
            <footer class="copyright navbar navbar-default navbar-bottom">
                <span>2015 © CHECKLIST TEAM. ALL RIGHT RESERVE.</span>
            </footer>
        </div>

        <!-- END LOGIN -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="../../assets/global/plugins/respond.min.js"></script>
        <script src="../../assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                $('[id^=detail-]').hide();
                $('.toggle').click(function () {
                    $input = $(this);
                    $target = $('#' + $input.attr('data-toggle'));
                    $target.slideToggle();
                });
            });
            $('.selectpicker').selectpicker();
            $('.selectpicker').selectpicker({
                style: 'btn-info',
                size: 4
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#mytable #checkall").click(function () {
                    if ($("#mytable #checkall").is(':checked')) {
                        $("#mytable input[type=checkbox]").each(function () {
                            $(this).prop("checked", true);
                        });

                    } else {
                        $("#mytable input[type=checkbox]").each(function () {
                            $(this).prop("checked", false);
                        });
                    }
                });

                $("[data-toggle=tooltip]").tooltip();
            });
        </script>
        <!-- add project javascript popover start here-->
        <!-- Modal -->
        <div class="modal fade" id="myModal12" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Project</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">Project name</label>
                                <div class="col-sm-7">
                                    <input type="text" name="project_name" class="form-control" id="inputEmail3" placeholder="enter project name">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- add project javascript popover end here-->
        <!-- suggest QC javascript popover start here-->
        <!-- Modal -->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Suggest QC</h4>
                    </div>
                    <div class="modal-body">
                            <label>Select Your QC Category</label>
                        <select class="form-control">
                            <option>body</option>
                            <option>footer</option>
                            <option>header</option>
                            <option>menu</option>
                            <option>content</option>
                        </select>
                              <br/>
                        <form class="form-horizontal">
                         
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-3 control-label">YOUR QC</label>
                                <div class="col-sm-7">
                                    <input type="text" name="user_qc" class="form-control" id="inputEmail3" placeholder="enter your qc">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <!--suggest QC javascript popover end here-->
    </body>
    <!-- END BODY -->
</html>