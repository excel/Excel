<?php
	
 namespace Quickqc\guidline;
 use Quickqc\Utility;
	 class Guidline {
		public $title = '';
		public $description = '';
		public $example = '';
		public $status = '';
		public $created = '';
		public $modified = '';
		public $created_by = '';
		public $modified_by = '';
		public $deleted_at = '';
		public $data = array(); 		// this variable use for return value
		private $conn = ''; 			// this variable use for database connection
		
		function __construct(){
			$this->conn = new \PDO('mysql:host=localhost;dbname=quickqc', 'root', '');
			$this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
		}	
		 
		public function create (){
			try {
				if($_SERVER['REQUEST_METHOD'] == 'POST'){
					$query = "INSERT INTO `quickqc`.`guidelines` (`title`, `description`) VALUES ('".$_POST['title']."', '".$_POST['description']."')"; 
					$this->conn->query($query);
					Utility :: redirect();
				}
				else {
					Utility :: redirect();
				}
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
	
			Utility :: redirect();

		}

		public function get ($id = null){
			try {
				
				$result = $this->conn->prepare('SELECT * FROM `guidelines` WHERE id = :id');
				$result->execute(array('id' => $id));
			 
				$row = $result->fetch();
				return $row;	
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
			
			
		}

		public function read (){
			try {
				$query = "SELECT * FROM `guidelines`"; 
				$result = $this->conn->query($query);
				
				foreach ($result as $row) {
					$this->data[] = $row;
				}
				return $this->data;
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
		}

		public function update (){
			
			try {
				
				if($_SERVER['REQUEST_METHOD'] == 'POST'){
					$query = "UPDATE `quickqc`.`guidelines` SET `title` = '".$_POST['title']."', `description` = '".$_POST['description']."' WHERE `guidelines`.`id` = ".$_POST['id'];
					$result = $this->conn->query($query);
					Utility :: redirect();
				}
				else {
					Utility :: redirect();
				}
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}
			
			Utility :: redirect();
		}

		public function delete (){
			
			try {
				$query = "DELETE FROM `quickqc`.`guidelines` WHERE `guidelines`.`id` = ".$_GET['id'];
				$this->conn->query($query);
			}
			catch(PDOException $e) {
				echo 'ERROR: ' . $e->getMessage();
			}

			Utility :: redirect();
		}

	}